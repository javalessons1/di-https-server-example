package org.example.app.manager;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.example.app.domain.User;
import org.example.app.dto.GetUserByIdRS;
import org.example.app.dto.UserRegisterRQ;
import org.example.app.dto.UserRegisterRS;
import org.example.app.exception.ItemNotFoundException;
import org.example.app.exception.LoginAlreadyRegisteredException;
import org.example.framework.di.annotation.Component;
import org.example.framework.security.annotation.Audit;
import org.example.framework.security.annotation.HasRole;
import org.example.framework.security.auth.AuthenticationToken;
import org.example.framework.security.auth.LoginPasswordAuthenticationToken;
import org.example.framework.server.context.SecurityContext;
import org.example.framework.server.context.AuthRQ;
import org.example.framework.server.exception.UnsupportAuthenticationToken;
import org.springframework.security.crypto.password.PasswordEncoder;

import java.util.HashMap;
import java.util.Map;
import java.util.Optional;
import java.util.concurrent.atomic.AtomicLong;

@Slf4j
@Component
@RequiredArgsConstructor
public class DefaultUserManager implements UserManager {
  private final AtomicLong nextId = new AtomicLong(1);
  private final Map<String, User> usersByLogin = new HashMap<>();
  private final Map<Long, User> usersById = new HashMap<>();
  private final PasswordEncoder encoder;
  @Audit
  @HasRole("ADMIN")
  public void test() {
    System.out.println("I AM ADMIN");
  }
  @Override
  public GetUserByIdRS getById(final long id) {
    final Optional<User> user;
    synchronized (this) {
      user = Optional.ofNullable(usersById.get(id));
    }

    return user
            .map(o -> new GetUserByIdRS(o.getId(), o.getLogin()))
            .orElseThrow(ItemNotFoundException::new)
            ;
  }

  @Override
  public UserRegisterRS create(final UserRegisterRQ requestDTO) {
    // TODO: check principal -> throw
    // TODO: check username - not admin (ROOT, administrator, support)
    // TODO: check for top 10_000 password
    final String login = requestDTO.getLogin().trim().toLowerCase();
    final String encodedPassword = encoder.encode(requestDTO.getPassword());

    final User user = User.builder()
        .id(nextId.getAndIncrement())
        .login(login)
        .passwordHash(encodedPassword)
        .build();

    synchronized (this) {
      if (usersByLogin.containsKey(login)) {
        log.error("registration with same login twice: {}", login);
        throw new LoginAlreadyRegisteredException();
      }
      usersByLogin.put(user.getLogin(), user);
      usersById.put(user.getId(), user);
    }

    return new UserRegisterRS(user.getId(), user.getLogin());
  }

  @Override
  public boolean authenticate(final AuthenticationToken request) {
    // TODO: bad code (обсудить, почему можно)
    if (!(request instanceof LoginPasswordAuthenticationToken)) {
      throw new UnsupportAuthenticationToken();
    }

    final LoginPasswordAuthenticationToken converted = (LoginPasswordAuthenticationToken) request;
    final String login = converted.getLogin();
    final String password = (String) converted.getCredentials();

    final String encodedPassword;
    synchronized (this) {
      // TODO: timing attack
      if (!usersByLogin.containsKey(login)) {
        SecurityContext.setAuth(AuthRQ.builder().role("ADMIN").login(login).build());
        return false;
      }

      encodedPassword = usersByLogin.get(login).getPasswordHash();
    }
    return encoder.matches(password, encodedPassword);
  }
}
