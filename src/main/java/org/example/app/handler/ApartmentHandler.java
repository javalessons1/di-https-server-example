package org.example.app.handler;

import com.google.gson.Gson;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.example.app.domain.Apartment;
import org.example.app.dto.*;
import org.example.app.exception.ApartmentNotFoundException;
import org.example.app.exception.AuthorizationException;
import org.example.app.manager.AvitoManager;
import org.example.framework.security.annotation.Audit;
import org.example.framework.security.annotation.HasRole;
import org.example.framework.server.http.Request;

import java.io.IOException;
import java.io.OutputStream;
import java.nio.charset.StandardCharsets;
import java.util.List;

@Slf4j
@RequiredArgsConstructor
public class ApartmentHandler {
    private static Gson gson;
    private static AvitoManager manager;

    @Audit
    @HasRole("ADMIN")
    public static void getAdminAll(final Request request, final OutputStream responseStream) throws IOException {
        final List<ApartmentRS> responseDTO = manager.getAll();
        final byte[] responseBody = gson.toJson(responseDTO).getBytes(StandardCharsets.UTF_8);
        writeResponse(responseStream, responseBody);
    }

    public void getAll(final Request request, final OutputStream responseStream) throws IOException {
        final List<ApartmentRS> responseDTO = manager.getAll();
        final byte[] responseBody = gson.toJson(responseDTO).getBytes(StandardCharsets.UTF_8);
        writeResponse(responseStream, responseBody);
    }

    public void create(final Request request, final OutputStream responseStream) throws IOException {
        try {
            final String requestBody = new String(request.getBody(), StandardCharsets.UTF_8);
            final ApartmentRQ requestDTO = gson.fromJson(requestBody, ApartmentRQ.class);
            final ApartmentRS responseDTO = manager.create(requestDTO);
            final byte[] responseBody = gson.toJson(responseDTO).getBytes(StandardCharsets.UTF_8);
            writeResponse(responseStream, responseBody);
        } catch (AuthorizationException e) {
            writeResponse(responseStream, ("Can't create item. Please authorize!").getBytes(StandardCharsets.UTF_8));
        }
    }

    public void getById(final Request request, final OutputStream responseStream) throws IOException {
        try {
            final String apartmentId = request.getPathGroup("apartmentId");
            try {
                final String id = apartmentId;
                final Apartment apartment = manager.getById(id);
                final byte[] responseBody = gson.toJson(apartment).getBytes(StandardCharsets.UTF_8);
                writeResponse(responseStream, responseBody);
            } catch (NumberFormatException e) {
                writeResponse(responseStream, ("Can't get id").getBytes(StandardCharsets.UTF_8));
            }
        } catch (ApartmentNotFoundException e) {
            writeResponse(responseStream, ("Apartment was removed or never existed").getBytes(StandardCharsets.UTF_8));
        }
    }

    public void update(final Request request, final OutputStream responseStream) throws IOException {
        try {
            final String requestBody = new String(request.getBody(), StandardCharsets.UTF_8);
            final ApartmentRQ updateRQ = gson.fromJson(requestBody, ApartmentRQ.class);
            final ApartmentRS updateRS = manager.update(updateRQ, request.getPathGroup("apartmentId"));
            final byte[] responseBody = gson.toJson(updateRS).getBytes(StandardCharsets.UTF_8);
            writeResponse(responseStream, responseBody);
        } catch (ApartmentNotFoundException e) {
            writeResponse(responseStream, ("Apartment was removed or never existed").getBytes(StandardCharsets.UTF_8));
        } catch (AuthorizationException e) {
            writeResponse(responseStream, ("Can't update item. Please authorize!").getBytes(StandardCharsets.UTF_8));
        }
    }

    public void delete(final Request request, final OutputStream responseStream) throws IOException {
        try {
            manager.removeById(request.getPathGroup("apartmentId"));
            writeResponse(responseStream, ("Apartment was removed successfully!").getBytes(StandardCharsets.UTF_8));
        } catch (ApartmentNotFoundException e) {
            writeResponse(responseStream, ("Apartment was removed or never existed").getBytes(StandardCharsets.UTF_8));
        } catch (AuthorizationException e) {
            writeResponse(responseStream, ("Can't update item. Please authorize!").getBytes(StandardCharsets.UTF_8));
        }
    }

    private static void writeResponse(OutputStream responseStream, byte[] body) throws IOException {
        responseStream.write((
                "HTTP/1.1 200 Ok\r\n" +
                        "Content-Length: " + body.length + "\r\n" +
                        "Connection: close\r\n" +
                        "Content-Type: application/json\r\n" +
                        "\r\n"
        ).getBytes(StandardCharsets.UTF_8));
        responseStream.write(body);
    }
}
