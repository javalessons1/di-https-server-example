package org.example.app.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.time.Instant;

@NoArgsConstructor
@AllArgsConstructor
@Data
public class ApartmentRS {
    private String id;
    private String roomAmount;
    private int price;
    private int size;
    private boolean withLoggia;
    private boolean withBalcony;
    private int floor;
    private int floorsInHouse;
    private Instant created;

}
