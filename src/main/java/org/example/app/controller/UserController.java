package org.example.app.controller;

import lombok.RequiredArgsConstructor;
import org.example.app.dto.GetUserByIdRS;
import org.example.app.dto.UserRegisterRQ;
import org.example.app.dto.UserRegisterRS;
import org.example.app.manager.UserManager;
import org.example.framework.di.annotation.Component;
import org.example.framework.server.annotation.*;
import org.example.framework.server.http.HttpMethods;


@Controller
@Component
@RequiredArgsConstructor
public class UserController {
  private final UserManager uManager;

  @RequestMapping(method = HttpMethods.GET, path = "^/users/(?<id>\\d+)$")
  @ResponseBody
  public GetUserByIdRS getById(@PathVariable("id") final long id) {
    return uManager.getById(id);
  }

  @RequestMapping(method = HttpMethods.POST, path = "^/users$")
  @ResponseBody
  public UserRegisterRS register(@RequestBody final UserRegisterRQ requestDTO) {
    return uManager.create(requestDTO);
  }

}
